import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import YoutubeEmbed from "./YoutubeEmbed";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "75%",
    height: "100%",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
};

export default function BasicModal(props) {
    const { open, onClose, content } = props;

    const temp = [];
    for (let index = 0; index < content.imgData[1]; index++) {
        temp.push(
            `/images/screenshots/${content.imgData[0]}/${content.imgData[0]}${index}.png`
        );
    }

    return (
        <Modal
            open={open}
            onClose={onClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableScrollLock={true}>
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2">
                    {content.title}
                </Typography>
                {content.url && (
                    <Typography
                        id="modal-modal-description"
                        align="center"
                        sx={{ mt: 2 }}>
                        lien du site :
                        <a
                            href={content.url}
                            target="_blank"
                            rel="noopener noreferrer">
                            {content.url}
                        </a>
                    </Typography>
                )}
                <Typography
                    id="modal-modal-description"
                    align="center"
                    sx={{ mt: 2 }}>
                    description : {content.description}
                </Typography>
                <Typography
                    id="modal-modal-stack"
                    align="center"
                    sx={{ mt: 2 }}>
                    stacks utilisés : {content.stack}
                </Typography>
                <Typography
                    id="modal-modal-none"
                    align="center"
                    sx={{ mt: 3 }}></Typography>

                {content.youtube && <YoutubeEmbed embedId={content.youtube} />}

                <div className="modal-screenshots">
                    {temp.map((image, index) => (
                        <img
                            src={image}
                            alt={`screenshot ${content.title} n° ${index}`}
                            className="screenshots"
                            key={index}
                        />
                    ))}
                </div>
            </Box>
        </Modal>
    );
}
