import React, { useEffect, useState } from "react";

import IconButton from "@mui/material/IconButton";

import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";

export default function Navbar(props) {
    const {scrollToSection} = props;

    const [burgerHide, setBurgerHide] = useState(true);

    useEffect(() => {
        setBurgerHide(true);
    }, []);

    return(
        <header>
                <nav className="navbar">
                    <div className="navname"  onClick={() => scrollToSection("title")}>
                        <img
                            className="logo"
                            src="/images/logos/avatar.png"
                            title="Nuurudiin Assock"
                            alt="mon avatar"
                        />
                        <h3>Nuurudiin Assock</h3>
                    </div>
                    <IconButton
                        className="burger"
                        onClick={() => setBurgerHide(!burgerHide)}>
                        {burgerHide ? <MenuIcon /> : <CloseIcon />}
                    </IconButton>
                    <div className="break"></div>
                    <ul className={burgerHide ? "navlist" : "navlist-burger"}>
                        <li onClick={() => scrollToSection("title")}>Accueil</li>
                        <li onClick={() => scrollToSection("profil")}>
                            Profil
                        </li>
                        <li onClick={() => scrollToSection("portfolio")}>
                            Portfolio
                        </li>
                        <li onClick={() => scrollToSection("contact")}>
                            Contact
                        </li>
                    </ul>
                </nav>
                <span
                    className="video-game-button back-to-top"
                    onClick={() => scrollToSection("title")}>
                    UP
                </span>
            </header>
    )
}