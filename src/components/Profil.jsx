import React from "react";

import Grid from "@mui/material/Grid";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";

import Avatar from "@mui/material/Avatar";

export default function Profil(props) {
    const { scrollToSection } = props;

    return (
        <section id="profil">
            <h1>🧙🏽Profil🦸🏽‍♂️</h1>
            <Grid container spacing={0}>
                <Grid
                    item
                    lg={3}
                    md={3}
                    xs={12}
                    order={{ lg: 1, md: 1, xs: 1 }}
                    className="flex-center">
                    <img
                        className="avatar"
                        src="/images/logos/avatar3.png"
                        title="Nuurudiin Assock"
                        alt="mon avatar"
                    />
                </Grid>

                <Grid
                    item
                    lg={3}
                    md={3}
                    xs={12}
                    order={{ lg: 2, md: 2, xs: 2 }}
                    className="flex-center">
                    <article className="flex-center">
                        <h2>Mes compétences</h2>
                        <List sx={{ width: "100%" }}>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/react.png"
                                            title="React"
                                            alt="logo React"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="React & React Native" />
                            </ListItem>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/nodejs.png"
                                            title="Node JS"
                                            alt="logo Node JS"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="Node JS & Express" />
                            </ListItem>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/mongodb.png"
                                            title="Mongo DB"
                                            alt="logo Mongo DB"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="MongoDB & Mongoose" />
                            </ListItem>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/mysql.png"
                                            title="MySQL"
                                            alt="logo MySQL"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="MySQL" />
                            </ListItem>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/heroku.png"
                                            title="Heroku"
                                            alt="logo Heroku"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/github.png"
                                            title="GitHub"
                                            alt="logo GitHub"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/unity.png"
                                            title="Unity"
                                            alt="logo Unity"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemAvatar>
                                    <Avatar sx={{ background: "white" }}>
                                        <img
                                            src="/images/logos/csharp.png"
                                            title="C#"
                                            alt="logo C#"
                                        />
                                    </Avatar>
                                </ListItemAvatar>
                            </ListItem>
                        </List>
                    </article>
                </Grid>

                <Grid
                    item
                    lg={6}
                    md={6}
                    xs={12}
                    order={{ lg: 2, md: 2, xs: 2 }}
                    className="flex-center">
                    <article className="flex-center presentation">
                        <h2>Présentation</h2>
                        <h3>
                            Hello ! Je m'appelle Nuurudiin, Bienvenue dans mon
                            univers ! 👨🏽‍💻 🎮
                        </h3>
                        <p>
                            Passionné d'informatique et de jeux vidéo, j'ai
                            toujours rêvé de devenir développeur !
                            <br />
                            Cependant comme dans tout bon RPG, la route à été
                            longue et compliquée, mais fort de mes nombreuses
                            aventures, j'ai acquis des compétences
                            intéressantes pour m'aider dans mon périple !
                            <br />
                            <br />
                            J'aimerais désormais utiliser ces compétences, et
                            les améliorer, en situation réelle, et c'est
                            pourquoi je suis actuellement à la recherche de mon
                            premier emploi en tant que développeur Web FullStack
                            JavaScript (React/NodeJS) junior !
                            <br />
                            <br />
                            Suite à mes différentes formations, j'ai pu réaliser
                            des projets, seul ou à plusieurs, dans le but de
                            valider mes connaissances, et de montrer ce dont je
                            suis capable ! Vous pouvez les retrouver ci-dessous,
                            dans la section Portfolio. N'hésitez pas à les
                            tester et à me faire vos retours, j'en serais ravi !
                            <br />
                            <br />
                            Fort de mes connaissances avancées sur les jeux
                            vidéo, ainsi que de plusieurs formations axées sur
                            le développement de ces derniers, j'ai aussi pu
                            acquérir de bonnes notions de gamification, que je
                            souhaite apporter au sein du web, pour ouvrir les
                            portes du net à tous !
                        </p>
                        <h3>
                            Car quoi de mieux qu'un jeu, pour tous nous
                            rapprocher ? 😁
                        </h3>
                    </article>
                </Grid>
            </Grid>
            <div className="btn-center">
                <span
                    className="start-btn"
                    onClick={() => scrollToSection("portfolio")}>
                    NEXT
                </span>
            </div>
        </section>
    );
}
