import { useState } from "react";

import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

import TabPanel from "./TabPanel";
import BasicModal from "./BasicModal";

export default function Portfolio(props) {
    const { scrollToSection } = props;

    const data = {
        web4all: {
            title: "Web4All",
            url: "https://web4all.netlify.app/",
            description:
                "Apprenez les langages de programmation du web en s'amusant ! 🎮 Site gratuit et ouvert à tous, testez les jeux sans inscription, ou créez un compte pour sauvegarder votre progression, et atteindre les sommets du Leaderboard ! 🥇",
            stack: "Front: React ⚛️, MaterialUI | Back: NodeJS, Express | BDD: MongoDB 🥭, Mongoose ",
            imgData: ["web4all", 4],
            youtube: "G_ivs305_yw",
        },
        noashop: {
            title: "Noashop",
            url: "https://noashop.netlify.app/",
            description:
                "Prototype de site de e-commerce (type fnac,amazon,etc..) 🛍️, avec une page d'accueil, une page produit pour chacun, un panier mis à jour en temps réel, ainsi que la possibilité d'ajouter un code promo (ex NOEL2021 pour avoir -50% sur le prix total). 🤑",
            stack: "Front: React ⚛️, Bootstrap | BDD: JSON stockés dans les local storage 💾",
            imgData: ["noashop", 4],
            youtube: "g3IWV3jCbWk",
        },
        sportapp: {
            title: "Sport App",
            description:
                "Application de sport 🏃 avec intégration de vidéos youtube, création de compte, suivi de progression 💪",
            stack: "Front: React-Native ⚛️, Expo | Back: NodeJS, Express | BDD: MongoDB 🥭",
            imgData: ["sportapp", 5],
            youtube: "rQaYNNMFBMg",
        },
        woolbot: {
            title: "Woolbot Runner",
            description:
                "Jeu mobile de type Infinite Runner 🏃, esquivez les lames 🗡️ pour survivre plus longtemps et améliorer votre record !",
            stack: "Unity, C#, Android SDK",
            imgData: ["woolbot", 0],
            youtube: "2eF6zaEDLa8",
        },
    };

    const [trigger, setTrigger] = useState({
        web4all: false,
        noashop: false,
        sportapp: false,
        woolbot: false,
    });

    const modalTrigger = (boolToTrigger) => {
        setTrigger({ ...trigger, [boolToTrigger]: !trigger[boolToTrigger] });
    };

    const [tab, setTab] = useState(0);

    const tabChange = (event, newValue) => {
        setTab(newValue);
    };

    function tabsProps(index) {
        return {
            id: `simple-tab-${index}`,
            "aria-controls": `simple-tabpanel-${index}`,
        };
    }

    return (
        <section id="portfolio">
            <h1>👨🏽‍💻Portfolio🎮</h1>

            <Box sx={{ width: "100%" }}>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                    <Tabs
                        value={tab}
                        onChange={tabChange}
                        centered
                        aria-label="basic tabs example">
                        <Tab label="Sites Web 👨🏽‍💻" {...tabsProps(0)} />
                        <Tab label="Applis Mobile 📱" {...tabsProps(1)} />
                        <Tab label="Jeux Vidéo 🎮" {...tabsProps(2)} />
                    </Tabs>
                </Box>
                <TabPanel value={tab} index={0} id="websites">
                    <article className="galerie">
                        <BasicModal
                            open={trigger.web4all}
                            content={data.web4all}
                            onClose={() => modalTrigger("web4all")}
                        />
                        <div
                            className="vignette"
                            onClick={() => modalTrigger("web4all")}>
                            <img
                                src="/images/screenshots/web4all/web4all0.png"
                                title="Web4All - Apprenez à coder en s'amusant !"
                                alt="https://web4all.netlify.app/"
                            />
                            <div className="caption">Web4All - Apprenez à coder en s'amusant !</div>
                        </div>

                        <BasicModal
                            open={trigger.noashop}
                            content={data.noashop}
                            onClose={() => modalTrigger("noashop")}
                        />
                        <div
                            className="vignette"
                            onClick={() => modalTrigger("noashop")}>
                            <img
                                src="/images/screenshots/noashop/noashop0.png"
                                title="Noashop - Site de e-commerce de jeux vidéo"
                                alt="https://noashop.netlify.app/"
                            />
                            <div className="caption">Noashop - Site de e-commerce de jeux vidéo</div>
                        </div>
                    </article>
                </TabPanel>
                <TabPanel value={tab} index={1} id="mobile-apps">
                    <article className="galerie">
                        <BasicModal
                            open={trigger.sportapp}
                            content={data.sportapp}
                            onClose={() => modalTrigger("sportapp")}
                        />
                        <div
                            className="vignette"
                            onClick={() => modalTrigger("sportapp")}>
                            <img
                                src="/images/screenshots/sportapp/sportapplogo.png"
                                title="Sport App - Application de sport avec vidéos"
                                alt="Sport App - Application de sport avec vidéos"
                            />
                            <div className="caption">Sport App - Application de sport avec vidéos</div>

                        </div>
                    </article>
                </TabPanel>
                <TabPanel value={tab} index={2}>
                    <article className="galerie" id="video-games">
                        <BasicModal
                            open={trigger.woolbot}
                            content={data.woolbot}
                            onClose={() => modalTrigger("woolbot")}
                        />
                        <div
                            className="vignette"
                            onClick={() => modalTrigger("woolbot")}>
                            <img
                                src="/images/screenshots/woolbot/woolbotlogo.png"
                                title="Woolbot Runner - Infinite runner sur mobile"
                                alt="Woolbot Runner - Infinite runner sur mobile"
                            />
                            <div className="caption">Woolbot Runner - Infinite runner sur mobile</div>
                        </div>
                    </article>
                </TabPanel>
            </Box>

            <div className="btn-center">
                <span
                    className="start-btn"
                    onClick={() => scrollToSection("contact")}>
                    NEXT
                </span>
            </div>
        </section>
    );
}
