import React from "react";
import { scroller } from "react-scroll";

import Box from "@mui/material/Box";

import Navbar from "./Navbar";
import Profil from "./Profil";
import Portfolio from "./Portfolio";
import Contact from "./Contact";

import "../style/home.css";

export default function Home() {

    const scrollToSection = (classToScroll) => {
        scroller.scrollTo(classToScroll, {
            duration: 1000,
            delay: 0,
            smooth: "easeInOutQuart",
        });
    };

    return (
        <Box sx={{ flexGrow: 1 }} wrap={"wrap"}>
            <Navbar scrollToSection={scrollToSection} />
            <main>
                <section id="title" className="flex-center full-screen">
                    <article className="flex-center title-text">
                        <h1>Nuurudiin Assock</h1>
                        <h2>Développeur FullStack JavaScript Junior</h2>
                    </article>
                    <span
                        className="start-btn"
                        onClick={() => scrollToSection("profil")}>
                        START
                    </span>
                </section>
                <div className="transitionA"></div>
                <Profil scrollToSection={scrollToSection} />
                <div className="transitionB"></div>
                <Portfolio scrollToSection={scrollToSection} />
                <div className="transitionA"></div>
                <Contact scrollToSection={scrollToSection} />
            </main>
                <footer className="footer">
                    <h2>© Nuurudiin ASSOCK 2022</h2>
                </footer>
        </Box>
    );
}
