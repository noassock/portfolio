export default function Contact() {

    return (
        <section id="contact">
            <h1>📲Contact📧</h1>
            <h2>📞 <a href="tel:0623698430">06.23.69.84.30</a></h2>
            <h2>📨 <a href="mailto:noassock@gmail.com">noassock@gmail.com</a></h2>
            <h2>📍 75015 Paris</h2>

            <h2>
                📝 &nbsp;
                <a href="/Nuurudiin-Assock-FullStack.pdf" target="_blank" rel="noopener noreferrer">
                    Curriculum Vitae
                </a>
            </h2>

            <h2>
                <a
                    href="https://www.linkedin.com/in/nuurudiin-assock/"
                    target="_blank"
                    rel="noopener noreferrer">
                    Linkedin
                </a>
            </h2>

            <h2>
            <a
                    href="https://github.com/NoaUnity"
                    target="_blank"
                    rel="noopener noreferrer">
                    GitHub
                </a>
            </h2>
            <h2>
            <a
                    href="https://gitlab.com/noassock"
                    target="_blank"
                    rel="noopener noreferrer">
                    GitLab
                </a>
            </h2>
        </section>
    );
}
